import java.util.Scanner;

public class Exercise09 {

    public static void main(String[] args) {
        inputText();
    }

    private static void inputText() {
        int index = 0;
        var count = 0;
        int letterLimit = 10;
        String text = new Scanner(System.in).nextLine();
        char[] textArray = text.toCharArray();
        for (int i = 0; i < text.length(); i++) {
            var symbol = textArray[i];
            try {
                if (Character.toUpperCase(symbol) < 65 || Character.toUpperCase(symbol) > 90) {
                    index += count + 1;
                    count = 0;
                } else {
                    count++;
                }
                if (count > letterLimit) {
                    while (Character.toUpperCase(textArray[i + 1]) >= 65 && Character.toUpperCase(textArray[i + 1]) <= 90 && i + 1 != text.length() - 1) {
                        i++;
                        count++;
                    }
                    if (i == (text.length() - 2)) {
                        i++;
                        count++;
                    }
                    throw new InputTextException("Word longer than " + letterLimit + " letters: ", text.substring(index, index + count));
                }
            } catch (InputTextException e) {
                System.err.print(e.getMessage());
                System.err.println(e.getWord());
            }
        }
    }
}