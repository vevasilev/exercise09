public class InputTextException extends Exception {
    private final String word;

    public String getWord() {
        return word;
    }

    public InputTextException(String message, String word) {
        super(message);
        this.word = word;
    }
}